[1]: https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
[2]: https://www.howtoforge.com/tutorial/centos-kubernetes-docker-cluster/
[3]: https://download.docker.com/linux/centos/8/x86_64/stable/Packages/
[4]: https://github.com/flannel-io/flannel
[5]: https://www.weave.works/docs/net/latest/overview/
[6]: https://stackoverflow.com/questions/47845739/configuring-flannel-to-use-a-non-default-interface-in-kubernetes
[7]: http://linuxsoft.cern.ch/centos/8-stream/isos/x86_64/
[8]: https://www.centos.org/
[9]: https://github.com/kubernetes/dashboard/blob/master/docs/user/installation.md/
[10]: https://www.rosehosting.com/blog/how-to-generate-a-self-signed-ssl-certificate-on-linux/
[11]: https://www.tecmint.com/install-nfs-server-on-centos-8/
[15]: https://www.jenkins.io/doc/book/installing/kubernetes/
[51]: https://kubernetes.io/docs/concepts/storage/volumes/
[52]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[70]: https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/
[90]: https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/

# Kubernetes Cluster administrieren

Ein Kubernetes-Cluster wird ausschlisslich über *https RESTful-API* (put,get,post,delete)administriert. Das gilt für *User Interaktionen* wie auch für den Cluster selbst.

## kubectl

Die *CLI* Administration erfolgt über das Kommando `kubectl`. 


## Kubernetes Starten / Stoppen


### drain


Mit `kubect drain <node>` kann ein Node, beispielsweise bei Maintenance, von laufenden Services geräumt werden. 

Dabei gibt es jedoch Restriktionen. Pod  die nicht über **ReplicationController, ReplicaSet, Job, DaemonSet oder StatefulSet** managed werden oder Pod die einen lokalen Storage verwenden, können mit `drain` nicht bedinet werden.

```
# kubectl drain k8s-node-2

# kubectl get no
NAME         STATUS                     ROLES                  AGE   VERSION
k8s-master   Ready                      control-plane,master   64d   v1.21.2
k8s-node-1   Ready                      <none>                 64d   v1.21.2
k8s-node-2   Ready,SchedulingDisabled   <none>                 64d   v1.21.2
```

### uncordon

Mit `kubectl uncordon <node> wird ein zuvor deaktivierter Node wieder aktiviert

```
# kubectl uncordon k8s-node-2
node/k8s-node-2 uncordoned

# kubectl get no
NAME         STATUS   ROLES                  AGE   VERSION
k8s-master   Ready    control-plane,master   64d   v1.21.2
k8s-node-1   Ready    <none>                 64d   v1.21.2
k8s-node-2   Ready    <none>                 64d   v1.21.2
```


## explain und describe

### explain

Mit explain wird eine beliebige Resource detailiert beschrieben.

#### Beispiel POD

`kubectl explain pod`

`kubectl explain pod.spec`

`kubectl explain pod.spec.containers`

### describe

Mit describe wird eine aktive Resource beschrieben.

#### Beispiel Node

Describe Node *k8s-master* 

`kubectl describe nodes k8s-master`

## Interaktionen mit Cluster

### Alles in allen Namespaces listen

`kubectl get all --all-namespaces`

### alle API Resourcen listen

`kubectl api-resources`

## Interaktionen mit Nodes

### detailierte Informationen über Nodes

`kubectl describe nodes`

## Interaktionen mit PODs

### detailierte Informationen über PODs

`kubectl describe pods`


### POD direkt starten 

` kubectl run hello-world-pd --image=gcr.io/google-samples/hello-app:1.0`

### POD über Deploymenet starten

`kubectl create deployment hello-world --image=gcr.io/google-samples/hello-app:1.0`


### DNS

Kubernets besitzt einen eigenen DNS Service. Die IP des DNS Service kann wie folgt ermittelt werden

```
# kubectl get services kube-dns -n kube-system
NAME       TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)                  AGE
kube-dns   ClusterIP   10.96.0.10   <none>        53/UDP,53/TCP,9153/TCP   79d
```


### POD IP und exposed Ports ermitteln

Eine Übersicht der laufenden Pods und deren IP erhlaten wir mit `kubectl get pods -o wide`.

```
# kubectl get pod  -o wide
NAME                                READY   STATUS    RESTARTS   AGE   IP             NODE         NOMINATED NODE   READINESS GATES
hello-world-5457b44555-p299z        1/1     Running   1          26d   10.244.1.147   k8s-node-1   <none>           <none>
hello-world-pd                      1/1     Running   1          26d   10.244.2.148   k8s-node-2   <none>           <none>
jenkins-675b6c5f6f-sqtg9            1/1     Running   0          11m   10.244.1.150   k8s-node-1   <none>           <none>
mysql-7f85754c74-jwc7v              1/1     Running   0          14d   10.244.2.151   k8s-node-2   <none>           <none>
nginx-deployment-66b6c48dd5-jf7xn   1/1     Running   1          22d   10.244.1.145   k8s-node-1   <none>           <none>
```

Um nun aber auch die exposed Ports zu erhalten, kann `kubectl get endpoints` benutzt werden.

```
# kubectl get endpoints
NAME            ENDPOINTS           AGE
jenkins         10.244.1.150:8080   15m
kubernetes      10.10.10.200:6443   78d
mysql           10.244.2.151:3306   14d
mysql-service   10.244.2.151:3306   36d
```

Mit dieser Information kann eine Service vom Cluster aus getestet werden.

```
# curl 10.244.1.150:8080
<html><head><meta http-equiv='refresh' content='1;url=/login?from=%2F'/><script>window.location.replace('/login?from=%2F');</script></head><body style='background-color:white; color:white;'>
```
### POD IP ermitteln und in Variable ablegen

Im folgenden Beispiel wird die IP des `jenkins` POD ermittelt und in der Shell-Variable `PODIP` gespeichert.

```
# POD=jenkins
# PODIP=$(kubectl get endpoints $POD -o jsonpath='{ .subsets[].addresses[].ip }')
# echo $PODIP
10.244.1.150
```

### Bash in POD aufrufen
 
 `kubectl exec -it hello-world-pd -- /bin/sh`

### POD Label anzeigen

Mit `kubectl get pods --show-labels` anzeigen.

```
# kubectl get pods --show-labels
NAME                                READY   STATUS    RESTARTS   AGE     LABELS
hello-world-5457b44555-p299z        1/1     Running   1          26d     app=hello-world,pod-template-hash=5457b44555
hello-world-pd                      1/1     Running   1          26d     run=hello-world-pd
jenkins-675b6c5f6f-6lkb7            1/1     Running   0          2m32s   app=jenkins,pod-template-hash=675b6c5f6f,run=jenkins
jenkins-675b6c5f6f-sqtg9            1/1     Running   0          28m     app=jenkins,pod-template-hash=675b6c5f6f,run=jenkins
mysql-7f85754c74-jwc7v              1/1     Running   0          14d     app=mysql,pod-template-hash=7f85754c74
nginx-deployment-66b6c48dd5-jf7xn   1/1     Running   1          22d     app=nginx,pod-template-hash=66b6c48dd5
```

### Environment Variablen anzeigen

Die gesetzen POS Environment Variablen können mit der Option `-- env` angezeigt werden, in diesem Beispiel für  `jenkins`.

```
# kubectl exec -it jenkins-675b6c5f6f-5m79k -- env |sort
COPY_REFERENCE_FILE_LOG=/var/jenkins_home/copy_reference_file.log
HOME=/var/jenkins_home
HOSTNAME=jenkins-675b6c5f6f-5m79k
JAVA_HOME=/opt/java/openjdk
JAVA_VERSION=jdk-11.0.11+9
JENKINS_ENABLE_FUTURE_JAVA=true
JENKINS_HOME=/var/jenkins_home
JENKINS_INCREMENTALS_REPO_MIRROR=https://repo.jenkins-ci.org/incrementals
JENKINS_PORT_8080_TCP_ADDR=10.102.247.231
JENKINS_PORT_8080_TCP_PORT=8080
JENKINS_PORT_8080_TCP_PROTO=tcp
JENKINS_PORT_8080_TCP=tcp://10.102.247.231:8080
JENKINS_PORT=tcp://10.102.247.231:8080
JENKINS_SERVICE_HOST=10.102.247.231
JENKINS_SERVICE_PORT=8080
JENKINS_SLAVE_AGENT_PORT=50000
JENKINS_UC_EXPERIMENTAL=https://updates.jenkins.io/experimental
JENKINS_UC=https://updates.jenkins.io
JENKINS_VERSION=2.289.3
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
KUBERNETES_PORT=tcp://10.96.0.1:443
KUBERNETES_SERVICE_HOST=10.96.0.1
KUBERNETES_SERVICE_PORT=443
KUBERNETES_SERVICE_PORT_HTTPS=443
LANG=en_US.UTF-8
LANGUAGE=en_US:en
LC_ALL=en_US.UTF-8
MYSQL_SERVICE_PORT_3306_TCP_ADDR=10.111.239.85
MYSQL_SERVICE_PORT_3306_TCP_PORT=3306
MYSQL_SERVICE_PORT_3306_TCP_PROTO=tcp
MYSQL_SERVICE_PORT_3306_TCP=tcp://10.111.239.85:3306
MYSQL_SERVICE_PORT=tcp://10.111.239.85:3306
MYSQL_SERVICE_SERVICE_HOST=10.111.239.85
MYSQL_SERVICE_SERVICE_PORT=3306
PATH=/opt/java/openjdk/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
REF=/usr/share/jenkins/ref
TERM=xterm
```
